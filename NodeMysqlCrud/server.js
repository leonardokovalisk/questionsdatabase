const express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  allowCors = require('./app/routes/cors');
  port = process.env.PORT || 3003;

app.listen(port);

console.log('API server started on: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(allowCors);

var routes = require('./app/routes/approutes');
routes(app);