-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Dificuldade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Dificuldade` (
  `idDificuldade` INT NOT NULL AUTO_INCREMENT,
  `nomeDificuldade` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idDificuldade`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Disciplina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Disciplina` (
  `idDisciplina` INT NOT NULL AUTO_INCREMENT,
  `nomeDisciplina` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idDisciplina`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`table1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Unidade` (
  `idUnidade` INT NOT NULL AUTO_INCREMENT,
  `nomeUnidade` VARCHAR(100) NULL,
  PRIMARY KEY (`idUnidade`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `nomeUsuario` VARCHAR(100) NOT NULL,
  `matricula` INT NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `telefone` VARCHAR(14) NOT NULL,
  `senha` VARCHAR(10) NOT NULL,
  `idDisciplina` INT NOT NULL,
  `idUnidade` INT NOT NULL,
  PRIMARY KEY (`idUsuario`),
  CONSTRAINT `fk_Usuario_Disciplina1`
    FOREIGN KEY (`idDisciplina`)
    REFERENCES `mydb`.`Disciplina` (`idDisciplina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_Unidade1`
    FOREIGN KEY (`idUnidade`)
    REFERENCES `mydb`.`Unidade` (`idUnidade`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Question` (
  `idQuestion` INT NOT NULL AUTO_INCREMENT,
  `enunciado` VARCHAR(5000) NOT NULL,
  `suporte` VARCHAR(5000) NOT NULL,
  `comando` VARCHAR(5000) NOT NULL,
  `alternativaA` VARCHAR(1000) NOT NULL,
  `alternativaB` VARCHAR(1000) NOT NULL,
  `alternativaC` VARCHAR(1000) NOT NULL,
  `alternativaD` VARCHAR(1000) NOT NULL,
  `alternativaE` VARCHAR(1000) NOT NULL,
  `gabarito` INT NOT NULL,
  `isValidated` BOOLEAN NOT NULL DEFAULT false,
  `idDificuldade` INT NOT NULL,
  `idDisciplina` INT NOT NULL,
  `idUsuario` INT NOT NULL,
  PRIMARY KEY (`idQuestion`),
  CONSTRAINT `fk_Question_Dificuldade1`
    FOREIGN KEY (`idDificuldade`)
    REFERENCES `mydb`.`Dificuldade` (`idDificuldade`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Question_Disciplina1`
    FOREIGN KEY (`idDisciplina`)
    REFERENCES `mydb`.`Disciplina` (`idDisciplina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Question_Usuario1`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `mydb`.`Usuario` (`idUsuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
