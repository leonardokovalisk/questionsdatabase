var Dificuldade = require('../model/dificuldadeModel')
var Disciplina = require('../model/disciplinaModel')
var Question = require('../model/questionModel')
var Unidade = require('../model/unidadeModel')
var Usuario = require('../model/userModel')

exports.list_all_dificuldade = function(req, res) {
  Dificuldade.getAllDificuldades(function(err, dificuldade) {
    console.log('controller')
    if (err) res.send(err)
    console.log('res', dificuldade)
    res.send(dificuldade)
  })
}

exports.create_dificuldade = function(req, res) {
  var new_dificuldade = new Dificuldade(req.body)

  if (!new_dificuldade.nomeDificuldade) {
    res.status(400).send({ error: true, message: 'Please provide name' })
  } else {
    Dificuldade.createDificuldade(new_dificuldade, function(err, dificuldade) {
      if (err) res.send(err)
      res.json(dificuldade)
    })
  }
}

exports.read_dificuldade = function(req, res) {
  Dificuldade.getDificuldadeById(req.params.dificuldadeId, function(err, dificuldade) {
    if (err) res.send(err)
    res.json(dificuldade)
  })
}

exports.update_dificuldade = function(req, res) {
  Dificuldade.updateById(req.params.dificuldadeId, new Dificuldade(req.body), function(err, dificuldade) {
    if (err) res.send(err)
    res.json(dificuldade)
  })
}

exports.delete_dificuldade = function(req, res) {
  Dificuldade.remove(req.params.dificuldadeId, function(err, dificuldade) {
    if (err) res.send(err)
    res.json({ message: 'Dificuldade successfully deleted' })
  })
}

exports.list_all_disciplina = function(req, res) {
  Disciplina.getAllDisciplinas(function(err, disciplina) {
    console.log('controller')
    if (err) res.send(err)
    console.log('res', disciplina)
    res.send(disciplina)
  })
}

exports.create_disciplina = function(req, res) {
  var new_disciplina = new Disciplina(req.body)

  if (!new_disciplina.nomeDisciplina) {
    res.status(400).send({ error: true, message: 'Please provide name' })
  } else {
    Disciplina.createDisciplina(new_disciplina, function(err, disciplina) {
      if (err) res.send(err)
      res.json(disciplina)
    })
  }
}

exports.read_disciplina = function(req, res) {
  Disciplina.getDisciplinaById(req.params.disciplinaId, function(err, disciplina) {
    if (err) res.send(err)
    res.json(disciplina)
  })
}

exports.update_disciplina = function(req, res) {
  Disciplina.updateById(req.params.disciplinaId, new Disciplina(req.body), function(err, disciplina) {
    if (err) res.send(err)
    res.json(disciplina)
  })
}

exports.delete_disciplina = function(req, res) {
  Disciplina.remove(req.params.disciplinaId, function(err, disciplina) {
    if (err) res.send(err)
    res.json({ message: 'Disciplina successfully deleted' })
  })
}

exports.list_all_question = function(req, res) {
  Question.getAllQuestions(function(err, question) {
    console.log('controller')
    if (err) res.send(err)
    console.log('res', question)
    res.send(question)
  })
}

exports.create_question = function(req, res) {
  var new_question = new Question(req.body)

  console.log(JSON.stringify(new_question))

  if (!new_question.enunciado) {
    res.status(400).send({ error: true, message: 'Please provide name' })
  } else {
    Question.createQuestion(new_question, function(err, question) {
      if (err) res.send(err)
      res.json(question)
    })
  }
}

exports.read_question = function(req, res) {
  Question.getQuestionById(req.params.questionId, function(err, question) {
    if (err) res.send(err)
    res.json(question)
  })
}

exports.update_question = function(req, res) {
  Question.updateById(req.params.questionId, new Question(req.body), function(err, question) {
    if (err) res.send(err)
    res.json(question)
  })
}

exports.delete_question = function(req, res) {
  Question.remove(req.params.questionId, function(err, question) {
    if (err) res.send(err)
    res.json({ message: 'Question successfully deleted' })
  })
}

exports.list_all_unidade = function(req, res) {
  Unidade.getAllUnidades(function(err, unidade) {
    console.log('controller')
    if (err) res.send(err)
    console.log('res', unidade)
    res.send(unidade)
  })
}

exports.create_unidade = function(req, res) {
  var new_unidade = new Unidade(req.body)

  if (!new_unidade.nomeUnidade) {
    res.status(400).send({ error: true, message: 'Please provide name' })
  } else {
    Unidade.createUnidade(new_unidade, function(err, unidade) {
      if (err) res.send(err)
      res.json(unidade)
    })
  }
}

exports.read_unidade = function(req, res) {
  Unidade.getUnidadeById(req.params.unidadeId, function(err, unidade) {
    if (err) res.send(err)
    res.json(unidade)
  })
}

exports.update_unidade = function(req, res) {
  Unidade.updateById(req.params.unidadeId, new Unidade(req.body), function(err, unidade) {
    if (err) res.send(err)
    res.json(unidade)
  })
}

exports.delete_unidade = function(req, res) {
  Unidade.remove(req.params.unidadeId, function(err, unidade) {
    if (err) res.send(err)
    res.json({ message: 'Unidade successfully deleted' })
  })
}

exports.list_all_usuario = function(req, res) {
  Usuario.getAllUsuarios(function(err, usuario) {
    console.log('controller')
    if (err) res.send(err)
    console.log('res', usuario)
    res.send(usuario)
  })
}

exports.create_usuario = function(req, res) {
  var new_usuario = new Usuario(req.body)

  if (!new_usuario.nomeUsuario) {
    res.status(400).send({ error: true, message: 'Please provide name' })
  } else {
    Usuario.createUsuario(new_usuario, function(err, usuario) {
      if (err) res.send(err)
      res.json(usuario)
    })
  }
}

exports.read_usuario = function(req, res) {
  Usuario.getUsuarioById(req.params.usuarioId, function(err, usuario) {
    if (err) res.send(err)
    res.json(Usuario)
  })
}

exports.update_usuario = function(req, res) {
  Usuario.updateById(req.params.usuarioId, new Usuario(req.body), function(err, usuario) {
    if (err) res.send(err)
    res.json(usuario)
  })
}

exports.delete_usuario = function(req, res) {
  Usuario.remove(req.params.usuarioId, function(err, usuario) {
    if (err) res.send(err)
    res.json({ message: 'Usuario successfully deleted' })
  })
}

exports.validateLogin = function(req, res) {
  Usuario.validateLogin(function(err, usuario) {
    if (err) res.send(err)
    res.json(usuario)
  })
}

exports.getValidate = function(req, res) {
  Question.getValidate(req.params.isValidated, function(err, question) {
    if (err) res.send(err)
    res.json(question)
  })
}

exports.putValidate = function(req, res) {
  console.log(req.params.isValidated, req.params.questionId)
  Question.putValidate(req.params.isValidated, req.params.questionId, function(err, question) {
    if (err) res.send(err)
    res.json(question)
  })
}
