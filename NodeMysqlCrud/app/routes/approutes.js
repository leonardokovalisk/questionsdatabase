module.exports = function(app) {
  var crudControlle = require('../controller/appController')

  app
    .route('/dificuldade')
    .get(crudControlle.list_all_dificuldade)
    .post(crudControlle.create_dificuldade)

  app
    .route('/dificuldade/:dificuldadeId')
    .get(crudControlle.read_dificuldade)
    .put(crudControlle.update_dificuldade)
    .delete(crudControlle.delete_dificuldade)

  app
    .route('/disciplina')
    .get(crudControlle.list_all_disciplina)
    .post(crudControlle.create_disciplina)

  app
    .route('/disciplina/:disciplinaId')
    .get(crudControlle.read_disciplina)
    .put(crudControlle.update_disciplina)
    .delete(crudControlle.delete_disciplina)
     
  app
    .route('/question')
    .get(crudControlle.list_all_question)
    .post(crudControlle.create_question)
    
  app
    .route('/question/:questionId')
    .get(crudControlle.read_question)
    .put(crudControlle.update_question)
    .delete(crudControlle.delete_question)

  app
    .route('/unidade')
    .get(crudControlle.list_all_unidade)
    .post(crudControlle.create_unidade)
    
  app
    .route('/unidade/:unidadeId')
    .get(crudControlle.read_unidade)
    .put(crudControlle.update_unidade)
    .delete(crudControlle.delete_unidade)
  
  app
    .route('/usuario')
    .get(crudControlle.list_all_usuario)
    .post(crudControlle.create_usuario)

  app
    .route('/usuario/:usuarioId')
    .get(crudControlle.read_usuario)
    .put(crudControlle.update_usuario)
    .delete(crudControlle.delete_usuario)

  app
    .route('/login')
    .get(crudControlle.validateLogin)
    
    app
      .route('/validate/:isValidated')
      .get(crudControlle.getValidate)
    
    app
      .route('/validate/:isValidated/:questionId')
      .put(crudControlle.putValidate)
  }
