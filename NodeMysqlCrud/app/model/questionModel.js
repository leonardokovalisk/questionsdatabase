var sql = require('./db.js')

var Question = function(question) {
  this.enunciado = question.enunciado
  this.suporte = question.suporte
  this.comando = question.comando
  this.alternativaA = question.alternativaA
  this.alternativaB = question.alternativaB
  this.alternativaC = question.alternativaC
  this.alternativaD = question.alternativaD
  this.alternativaE = question.alternativaE
  this.gabarito = question.gabarito
  this.idDificuldade = question.idDificuldade
  this.idDisciplina = question.idDisciplina
  this.idUsuario = question.idUsuario
}
Question.createQuestion = function(newQuestion, result) {
  sql.query('INSERT INTO Question set ?', newQuestion, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      console.log(res.insertId)
      result(null, res.insertId)
    }
  })
}
Question.getQuestionById = function(questionId, result) {
  sql.query('Select * from Question where idQuestion = ? ', questionId, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      result(null, res)
    }
  })
}
Question.getAllQuestions = function(result) {
  sql.query('Select q.idQuestion, q.enunciado, q.suporte, q.comando, q.alternativaA, q.alternativaB, q.alternativaC, q.alternativaD, q.alternativaE, q.gabarito, d.nomeDisciplina, u.nomeUsuario, t.nomeDificuldade from Question q inner join Disciplina d on q.idDisciplina = d.idDisciplina inner join Usuario u on q.idUsuario = u.idUsuario inner join Dificuldade t on q.idDificuldade = t.idDificuldade', function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      console.log('tasks : ', res)

      result(null, res)
    }
  })
}
Question.updateById = function(id, task, result) {
  sql.query('UPDATE Question SET enunciado = ?, suporte = ?, comando = ?, alternativaA = ?, alternativaB = ?, alternativaC = ?, alternativaD = ?, alternativaE = ?, gabarito = ?, idDificuldade = ?, idDisciplina = ?, idQuestion = ? id WHERE idQuestion = ?', [question.enunciado, question.suporte, question.comando, question.alternativaA, question.alternativaB, question.alternativaC, question.alternativaD, question.alternativaE, question.gabarito, question.idDificuldade, question.idDisciplina, question.idQuestion, id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}
Question.remove = function(id, result) {
  sql.query('DELETE FROM Question WHERE idQuestion = ?', [id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}
Question.getValidate = function(isValidated, result) {
  sql.query('Select q.idQuestion, q.enunciado, q.suporte, q.comando, q.alternativaA, q.alternativaB, q.alternativaC, q.alternativaD, q.alternativaE, q.gabarito, q.isValidated, d.nomeDisciplina, u.nomeUsuario, t.nomeDificuldade from Question q inner join Disciplina d on q.idDisciplina = d.idDisciplina inner join Usuario u on q.idUsuario = u.idUsuario inner join Dificuldade t on q.idDificuldade = t.idDificuldade where isValidated = ? ', isValidated, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      result(null, res)
    }
  })
}
Question.putValidate = function(isValidated, id, result) {
  sql.query('UPDATE Question SET isValidated = ? WHERE idQuestion = ?', [isValidated, id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}

module.exports = Question
