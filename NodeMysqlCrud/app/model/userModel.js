var sql = require('./db.js')

var Usuario = function(usuario) {
  this.nomeUsuario = usuario.nomeUsuario
  this.matricula = usuario.matricula
  this.email = usuario.email
  this.telefone = usuario.telefone
  this.senha = usuario.senha
  this.idDisciplina = usuario.idDisciplina
  this.idUnidade = usuario.idUnidade
}
Usuario.createUsuario = function(novaUsuario, result) {
  sql.query('INSERT INTO Usuario set ?', novaUsuario, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      console.log(res.insertId)
      result(null, res.insertId)
    }
  })
}
Usuario.getUsuarioById = function(usuarioId, result) {
  sql.query('Select u.nomeUsuario, u.matricula, u.email, u.telefone, d.nomeDisciplina, t.nomeUnidade from Usuario u inner join Disciplina d on u.idDisciplina = d.idDisciplina inner join Unidade t on u.idUnidade = t.idUnidade where idUsuario = ? ', usuarioId, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      result(null, res)
    }
  })
}
Usuario.getAllUsuarios = function(result) {
  sql.query('Select u.idUsuario, u.nomeUsuario, u.matricula, u.email, u.telefone, d.nomeDisciplina, t.nomeUnidade from Usuario u inner join Disciplina d on u.idDisciplina = d.idDisciplina inner join Unidade t on u.idUnidade = t.idUnidade', function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      console.log('tasks : ', res)

      result(null, res)
    }
  })
}
Usuario.updateById = function(id, usuario, result) {
  sql.query('UPDATE Usuario SET nomeUsuario, matricula, email, telefone, senha, idDisciplina, idUnidade = ? WHERE idUsuario = ?', [usuario.nomeUsuario, usuario.matricula, usuario.email, usuario.telefone, usuario.senha, usuario.idDisciplina, usuario.idUnidade, id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}
Usuario.remove = function(id, result) {
  sql.query('DELETE FROM Usuario WHERE idUsuario = ?', [id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}

Usuario.validateLogin = function(result){
  sql.query('Select email, senha FROM Usuario', function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}

module.exports = Usuario
