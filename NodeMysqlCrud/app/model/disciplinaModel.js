var sql = require('./db.js')

var Disciplina = function(disciplina) {
  this.nomeDisciplina = disciplina.nomeDisciplina
}
Disciplina.createDisciplina = function(novaDisciplina, result) {
  sql.query('INSERT INTO Disciplina set ?', novaDisciplina, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      console.log(res.insertId)
      result(null, res.insertId)
    }
  })
}
Disciplina.getDisciplinaById = function(disciplinaId, result) {
  sql.query('Select idDisciplina, nomeDisciplina from Disciplina where idDisciplina = ? ', disciplinaId, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      result(null, res)
    }
  })
}
Disciplina.getAllDisciplinas = function(result) {
  sql.query('Select * from Disciplina', function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      console.log('tasks : ', res)

      result(null, res)
    }
  })
}
Disciplina.updateById = function(id, disciplina, result) {
  sql.query('UPDATE Disciplina SET nomeDisciplina = ? WHERE idDisciplina = ?', [disciplina.nomeDisciplina, id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}
Disciplina.remove = function(id, result) {
  sql.query('DELETE FROM Disciplina WHERE idDisciplina = ?', [id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}

module.exports = Disciplina
