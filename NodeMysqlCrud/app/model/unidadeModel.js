var sql = require('./db.js')

var Unidade = function(unidade) {
  this.nomeUnidade = unidade.nomeUnidade
}
Unidade.createUnidade = function(novaUnidade, result) {
  sql.query('INSERT INTO Unidade set ?', novaUnidade, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      console.log(res.insertId)
      result(null, res.insertId)
    }
  })
}
Unidade.getUnidadeById = function(unidadeId, result) {
  sql.query('Select * from Unidade where idUnidade = ? ', unidadeId, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      result(null, res)
    }
  })
}
Unidade.getAllUnidades = function(result) {
  sql.query('Select * from Unidade', function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      console.log('tasks : ', res)

      result(null, res)
    }
  })
}
Unidade.updateById = function(id, unidade, result) {
  sql.query('UPDATE Unidade SET nomeUnidade = ? WHERE idUnidade = ?', [unidade.nomeUnidade, id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}
Unidade.remove = function(id, result) {
  sql.query('DELETE FROM Unidade WHERE idUnidade = ?', [id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}

module.exports = Unidade
