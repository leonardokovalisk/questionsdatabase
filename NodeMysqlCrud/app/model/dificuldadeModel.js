var sql = require('./db.js')

var Dificuldade = function(dificuldade) {
  this.nomeDificuldade = dificuldade.nomeDificuldade
}
Dificuldade.createDificuldade = function(novaDificuldade, result) {
  sql.query('INSERT INTO Dificuldade set ?', novaDificuldade, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      console.log(res.insertId)
      result(null, res.insertId)
    }
  })
}
Dificuldade.getDificuldadeById = function(dificuldadeId, result) {
  sql.query('Select idDificuldade, nomeDificuldade from Dificuldade where idDificuldade = ? ', dificuldadeId, function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(err, null)
    } else {
      result(null, res)
    }
  })
}
Dificuldade.getAllDificuldades = function(result) {
  sql.query('Select * from Dificuldade', function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      console.log('tasks : ', res)

      result(null, res)
    }
  })
}
Dificuldade.updateById = function(id, dificuldade, result) {
  sql.query('UPDATE Dificuldade SET nomeDificuldade = ? WHERE idDificuldade = ?', [dificuldade.nomeDificuldade, id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}
Dificuldade.remove = function(id, result) {
  sql.query('DELETE FROM Dificuldade WHERE idDificuldade = ?', [id], function(err, res) {
    if (err) {
      console.log('error: ', err)
      result(null, err)
    } else {
      result(null, res)
    }
  })
}

module.exports = Dificuldade
