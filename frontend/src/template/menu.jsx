import React, { Component } from 'react'

import Grid from './grid'
import './custom.css'

export default class Menu extends Component {
  render() {
    return (
      <Grid cols=" ">
        <nav className="colorMenu">
          <div className="container">
            <div>
              <a className="navbar-brand" href="#">
                <img src="assents/SENAI_LOGO.png" alt="senaiLogo" width="100px" />
              </a>
            </div>

            <div>
              <ul className="nav navbar-nav">
                <li>
                  <a href="#/question">Cadastrar Questões</a>
                </li>
                <li>
                  <a href="#/viewQuestion">Visualizar Questões</a>
                </li>
                <li>
                  <a href="#/usuario">Cadastrar Usuario</a>
                </li>
                <li>
                  <a href="#/viewUsuario">Visualizar Usuario</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </Grid>
    )
  }
}
