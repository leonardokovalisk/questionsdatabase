import React from 'react'
import { Router, Route, Redirect, hashHistory } from 'react-router'

import addQuestion from '../components/addQuestion'
import viewQuestion from '../components/viewQuestion'
import addUsuario from '../components/addUser'
import viewUsuario from '../components/viewUser'
import Home from '../components/home'

export default props => (
  <Router history={hashHistory}>
    <Route path="/question" component={addQuestion} />
    <Route path="/viewQuestion" component={viewQuestion} />
    <Route path="/usuario" component={addUsuario} />
    <Route path="/viewUsuario" component={viewUsuario} />
    <Route path='/question/:questionId?' component={addQuestion} />
    <Route path='/home' component={Home} />
    <Redirect from="*" to="/home" />
  </Router>
)
