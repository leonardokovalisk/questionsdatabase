import React, { Component } from 'react'
import axios from 'axios'

import PageHeader from '../template/pageHeader'

export default class ViewUser extends Component {
  constructor(props) {
    super(props)

    this.state = {
      users: []
    }

    this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    axios.get("http://localhost:3003/usuario")
      .then(response => {
        this.setState({ users: response.data });
      });
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }


  render() {
    return (
      <div>
        <PageHeader name="Visualizar Usuários" small="" />
        {this.state.users.map(user =>
          <div key={user.idUsuario}>
            <p><b>Nome do Usuário: </b>{user.nomeUsuario}</p>
            <p><b>Matricula: </b>{user.matricula}</p>
            <p><b>Telefone: </b>{user.telefone}</p>
            <p><b>Email: </b>{user.email}</p>
            <p><b>Disciplina: </b>{user.nomeDisciplina}</p>
            <p><b>Unidade de Atuação: </b>{user.nomeUnidade}</p>
            <br />
          </div>
        )}
      </div>
    )
  }


}