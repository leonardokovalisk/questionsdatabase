import React, { Component } from 'react'
import axios from 'axios'

import PageHeader from '../template/pageHeader'

export default class AddQuestion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      idQuestion: null,
      enunciado: "",
      suporte: "",
      comando: "",
      alternativaA: "",
      alternativaB: "",
      alternativaC: "",
      alternativaD: "",
      alternativaE: "",
      gabarito: "",
      idDificuldade: 1,
      idDisciplina: 1,
      idUsuario: 1,
      mensagem: "",
      users: [],
      disciplinas: []
    };

    this.onChange = this.onChange.bind(this);
    this.salvar = this.salvar.bind(this);
  }

  componentWillMount() {
    axios.get("http://localhost:3003/usuario")
      .then(response => {
        this.setState({ users: response.data });
      });

    axios.get("http://localhost:3003/disciplina")
      .then(response => {
        this.setState({ disciplinas: response.data });
      });
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  salvar(e) {
    e.preventDefault();

    const question = {
      idQuestion: this.state.idQuestion,
      enunciado: this.state.enunciado,
      suporte: this.state.suporte,
      comando: this.state.comando,
      alternativaA: this.state.alternativaA,
      alternativaB: this.state.alternativaB,
      alternativaC: this.state.alternativaC,
      alternativaD: this.state.alternativaD,
      alternativaE: this.state.alternativaE,
      gabarito: this.state.gabarito,
      idDificuldade: this.state.idDificuldade,
      idDisciplina: this.state.idDisciplina,
      idUsuario: this.state.idUsuario
    }

    axios
      .post('http://localhost:3003/question',
        question)
      .then(res => {
        this.setState({
          idQuestion: null,
          enunciado: "",
          suporte: "",
          comando: "",
          alternativaA: "",
          alternativaB: "",
          alternativaC: "",
          alternativaD: "",
          alternativaE: "",
          gabarito: "",
          idDificuldade: 1,
          idDisciplina: 1,
          idUsuario: 1,
          mensagem: "Questão salva com sucesso!"
        });
      })
  }

  render() {
    return (
      <div>
        <PageHeader name="Cadastro de Questões" small="" />

        {this.state.mensagem && (
          <div>{this.state.mensagem}</div>
        )}
        <form onSubmit={this.salvar}>

          <label>Enunciado: </label>
          <textarea className="textarea" placeholder="Enunciado" name="enunciado" onChange={this.onChange} value={this.state.enunciado}></textarea>

          <label>Suporte: </label>
          <textarea className="textarea" placeholder="Suporte" name="suporte" onChange={this.onChange} value={this.state.suporte}></textarea>

          <label>Comando: </label>
          <textarea className="textarea" placeholder="Comando" name="comando" onChange={this.onChange} value={this.state.comando}></textarea>

          <label>Alternativa A: </label>
          <input type="text" className="input is-primary" name="alternativaA" onChange={this.onChange} value={this.state.alternativaA} />
          <br />

          <label>Alternativa B: </label>
          <input type="text" className="input is-primary" name="alternativaB" onChange={this.onChange} value={this.state.alternativaB} />
          <br />

          <label>Alternativa C: </label>
          <input type="text" className="input is-primary" name="alternativaC" onChange={this.onChange} value={this.state.alternativaC} />
          <br />

          <label>Alternativa D: </label>
          <input type="text" className="input is-primary" name="alternativaD" onChange={this.onChange} value={this.state.alternativaD} />
          <br />

          <label>Alternativa E: </label>
          <input type="text" className="input is-primary" name="alternativaE" onChange={this.onChange} value={this.state.alternativaE} />
          <br /><br />

          <label>Gabarito: </label>
          <div className="field">
            <div className="control">
              <div className="select is-primary">
                <select name="gabarito" form="carform" onChange={this.onChange} value={this.state.gabarito}>
                  <option value="1">A</option>
                  <option value="2">B</option>
                  <option value="3">C</option>
                  <option value="4">D</option>
                  <option value="5">E</option>
                </select>
              </div></div></div>

          <label>Dificuldade: </label>
          <div className="field">
            <div className="control">
              <div className="select is-primary">
                <select name="idDificuldade" className="select" form="carform" onChange={this.onChange} value={this.state.idDificuldade}>
                  <option value="1">Fácil</option>
                  <option value="2">Médio</option>
                  <option value="3">Difícil</option>
                </select>
              </div></div></div>

          <label>Disciplina: </label>
          <div className="field">
            <div className="control">
              <div className="select is-primary">
                <select name="idDisciplina" className="select" form="carform" onChange={this.onChange} value={this.state.idDisciplina}>
                  {this.state.disciplinas.map(disciplina =>
                    <option key={disciplina.idDisciplina} value={disciplina.idDisciplina}>{disciplina.nomeDisciplina}</option>
                  )}
                </select>
              </div></div></div>

          <label>Autor: </label>
          <div className="field">
            <div className="control">
              <div className="select is-primary">
                <select name="idUsuario" className="select" form="carform" onChange={this.onChange} value={this.state.idUsuario}>
                  {this.state.users.map(user =>
                    <option key={user.idUsuario} value={user.idUsuario}>{user.nomeUsuario}</option>
                  )}
                </select>
              </div></div></div>
          <br />

          <button className='button is-primary' type="Submit">Salvar</button>
          <br /><br />

        </form>

      </div >
    )
  }
}
