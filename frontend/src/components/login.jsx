import React, { Component } from 'react'
import axios from 'axios'

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      senha: ''
    };

    this.onChange = this.onChange.bind(this);
    this.verificarLogin = this.verificarLogin.bind(this)
  }

  onChange(e) {
    this.setState({
        [e.target.name]: e.target.value
    });
  }

  verificarLogin(e) {
    e.preventDefault();

    const login = {
      email: this.state.email,
      senha: this.state.senha,
    }
    
    axios
      .get('http://localhost:3003/login')
      .then(res => {
          this.setState({
            email: "",
            senha: "",
          });
          var result = res.data
          for (const key in result) {
            if (result.hasOwnProperty(key)) {
              const element = result[key];
              if (element.email == login.email && element.senha == login.senha) {
              window.location.href = '#/home'
              } else {
                return;
              }
            }
          }
      })
    }

  render() {
    return (
      <div>
        <form onSubmit={this.verificarLogin}>

          <label>Email: </label>
          <input type="email" name="email" onChange={this.onChange} value={this.state.email}/>
          <br/>

          <label>Senha: </label>
          <input type="password" name="senha" onChange={this.onChange} value={this.state.senha}/>
          <br/>

          <button type="Submit">Login</button>

        </form>

      </div>
    )
  }
}
