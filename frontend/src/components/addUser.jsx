import React, { Component } from 'react'
import axios from 'axios'

import PageHeader from '../template/pageHeader'

export default class AddUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      idUsuario: null,
      nomeUsuario: "",
      matricula: "",
      email: "",
      telefone: "",
      senha: "",
      idDisciplina: 1,
      idUnidade: 1,
      mensagem: "",
      unidades: [],
      disciplinas: []
    };

    this.onChange = this.onChange.bind(this);
    this.salvar = this.salvar.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  componentWillMount() {
    axios.get("http://localhost:3003/unidade")
      .then(response => {
        this.setState({ unidades: response.data });
      });

    axios.get("http://localhost:3003/disciplina")
      .then(response => {
        this.setState({ disciplinas: response.data });
      });
  }

  salvar(e) {
    e.preventDefault();

    const user = {
      idUsuario: this.state.idUsuario,
      nomeUsuario: this.state.nomeUsuario,
      matricula: this.state.matricula,
      email: this.state.email,
      telefone: this.state.telefone,
      senha: this.state.senha,
      idDisciplina: this.state.idDisciplina,
      idUnidade: this.state.idUnidade
    }

    axios
      .post('http://localhost:3003/usuario',
        user)
      .then(res => {
        this.setState({
          idUsuario: null,
          nomeUsuario: "",
          matricula: "",
          email: "",
          telefone: "",
          senha: "",
          idDisciplina: null,
          idUnidade: null,
          mensagem: "Usuário salva com sucesso!"
        });
      })
  }

  render() {
    return (
      <div>
        <PageHeader name="Cadastro de Usuário" small="" />

        {this.state.mensagem && (
          <div>{this.state.mensagem}</div>
        )}
        <form onSubmit={this.salvar}>

          <label>Nome do Usuario: </label>
          <input type="text" className="input" name="nomeUsuario" onChange={this.onChange} value={this.state.nomeUsuario} />

          <label>Matricula: </label>
          <input type="text" className="input" name="matricula" onChange={this.onChange} value={this.state.matricula} />

          <label>Telefone: </label>
          <input type="tel" className="input" name="telefone" placeholder="Ex: 99999-9999" pattern="[0-9]{5}-[0-9]{4}" onChange={this.onChange} value={this.state.telefone} />
          <label>Email: </label>
          <input type="email" className="input" name="email" onChange={this.onChange} value={this.state.email} />

          <label>Senha: </label>
          <input type="password" className="input" name="senha" onChange={this.onChange} value={this.state.senha} />
          <br /><br />

          <label>Disciplina: </label>
          <div className="field">
            <div className="control">
              <div className="select is-primary">
                <select name="idDisciplina" className="select" form="carform" onChange={this.onChange} value={this.state.idDisciplina}>
                  {this.state.disciplinas.map(disciplina =>
                    <option key={disciplina.idDisciplina} value={disciplina.idDisciplina}>{disciplina.nomeDisciplina}</option>
                  )}
                </select>
              </div></div></div>

          <label>Unidade de Atuação: </label>
          <div className="field">
            <div className="control">
              <div className="select is-primary">
                <select name="idUnidade" className="select" form="carform" onChange={this.onChange} value={this.state.idUnidade}>
                  {this.state.unidades.map(unidade =>
                    <option key={unidade.idUnidade} value={unidade.idUnidade}>{unidade.nomeUnidade}</option>
                  )}
                </select>
              </div></div></div>
          <br />

          <button className='button is-primary' type="Submit">Salvar</button>
          <br /><br />

        </form>

      </div >
    )
  }
}
