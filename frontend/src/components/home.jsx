import React, { Component } from 'react'

export default class Home extends Component {
    render() {
        return (
            <div className="home" >
                <p className="titulo" ><b>Cadastro de Questões fácil e rápido!</b></p>
                <button className="button is-success is-inverted btn-home" onClick={() => window.location.href = '#/question'}>Cadastre Aqui</button>
            </div>
        )
    }
}