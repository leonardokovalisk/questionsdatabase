import React, { Component } from 'react'
import axios from 'axios'

import PageHeader from '../template/pageHeader'

export default class ViewQuestion extends Component {
  constructor(props) {
    super(props)

    this.state = {
      questions: [],
      validated: 1
    }

    this.onChange = this.onChange.bind(this);
    this.changeView = this.changeView.bind(this);
    this.isValidated = this.isValidated.bind(this);
    this.updateValidate = this.updateValidate.bind(this);
  }

  componentWillMount() {
    axios.get(`http://localhost:3003/validate/${this.state.validated}`)
      .then(response => {
        this.setState({ questions: response.data });
      });
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  updateValidate(idQuestion) {
    axios.put(`http://localhost:3003/validate/${this.state.validated}/${idQuestion}`)
      .then(response => {
        this.setState({ questions: response.data });
      })
  }

  isValidated(e) {
    axios.get(`http://localhost:3003/validate/${e.target.value}`)
      .then(response => {
        this.setState({ questions: response.data })
      })
  }

  changeView() {
    let gabaritoView = ''

    for (const key in this.state.questions) {
      if (this.state.questions.hasOwnProperty(key)) {
        const element = this.state.questions[key];
        switch (element.gabarito) {
          case 1:
            gabaritoView = 'A'
            break;
          case 2:
            gabaritoView = 'B'
            break;
          case 3:
            gabaritoView = 'C'
            break;
          case 4:
            gabaritoView = 'D'
            break;
          case 5:
            gabaritoView = 'E'
            break;
          default:
            break;
        }
      }
    }

    return gabaritoView
  }


  render() {
    return (
      <div>
        <PageHeader name="Visualizar Questões" small="" />
        <form>
          <div className="field">
            <div className="control">
              <div className="select is-primary">
                <select name="isValidated" className="select" form="carform" onChange={this.isValidated}>
                  <option value='1'>Validadas</option>
                  <option value='0'>Não Validadas</option>
                </select>
              </div></div></div>
          <br />
          {this.state.questions.map(question =>
            <div key={question.idQuestion}>
              <p><b>Enunciado: </b>{question.enunciado}</p>
              {question.suporte ? <p><b>Suporte: </b>{question.suporte}</p> : null}
              {question.comando ? <p><b>Comando: </b>{question.comando}</p> : null}
              <p><b>Gabarito: </b>{this.changeView()}</p>
              <p><b>Dificuldade: </b>{question.nomeDificuldade}</p>
              <p><b>Disciplina: </b>{question.nomeDisciplina}</p>
              <p><b>Autor: </b>{question.nomeUsuario}</p>
              {question.isValidated == 0 ? <button className="button is-info" onClick={() => this.updateValidate(question.idQuestion)}>Validar</button> : null}
              <br /><br />
            </div>
          )}
        </form>
      </div>
    )
  }

}